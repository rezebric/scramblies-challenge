(defproject scramblies "0.1.0-SNAPSHOT"
  :description "Flexiana Scramblies Challenge"
  :url "https://bitbucket.org/rezebric/scramblies-challenge"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-json "0.5.0"]
                 [org.clojure/clojurescript "1.10.764"]
                 [cljs-http "0.1.46"]
                 [org.clojure/core.async "1.3.610"]
                 [org.clojars.magomimmo/domina "2.0.0-SNAPSHOT"]]
  :plugins [[lein-ring "0.12.5"]
            [lein-cljsbuild "1.1.8"]]
  :ring {:handler scramblies.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}}

  :cljsbuild {:builds [{:source-paths ["src-cljs"]
                        :compiler {:output-to "resources/public/main.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]})
