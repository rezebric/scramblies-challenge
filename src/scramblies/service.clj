(ns scramblies.service)

(defn is-input-valid?
  "Checks input according to the given function"
  [fn & input]
  (every? #(fn %) input))

(defn is-lower-case?
  "Checks if a string has only lower case letters"
  [str]
  (every? #(Character/isLowerCase %) str))

(defn scramble?
  "Returns true if a portion of str1 characters can be rearranged
  to match str2, otherwise returns false"
  [str1 str2]
  (let [map1 (frequencies str1)
        map2 (frequencies str2)]
    (every? #(let [[letter frequency2] %
                   frequency1 (get map1 letter 0)]
               (>= frequency1 frequency2)) map2)))

(def msgs
  {:100 "You did it!!! Words are scramblies."
   :200 "Words are not scramblies. Try again."
   :300 (str "Invalid input " (-> 0x1F61D Character/toChars String.))})

(defn msg [error-code]
  {:code (Integer. error-code) :msg ((keyword error-code) msgs)})

(defn scramble
  "Checks if the strings str1 and str2 can be scrambled"
  [str1 str2]
  (if (is-input-valid? is-lower-case? str1 str2)
    (if (scramble? str1 str2) (msg "100") (msg "200"))
    (msg "300")))