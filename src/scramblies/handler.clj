(ns scramblies.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :as ring-json]
            [ring.util.response :as rr]
            [scramblies.service :as scr]))

(defroutes app-routes
  (GET "/" [] (rr/response {:service "/scramblies" :params ["str1" "str2"]}))
  (GET "/scramblies" [str1 str2] (rr/response (scr/scramble str1 str2)))
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (ring-json/wrap-json-response)
      (wrap-defaults site-defaults)))
