(ns scramblies.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [domina.core :refer [by-id value set-text! add-class! remove-class!]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]))

(def ack-el "ack")
(def result-el "result")

(defn toggle-class
  "Add remove the style class of an element"
  [id code]
  (case code
    :success (do
               (remove-class! (by-id id) "alert-danger")
               (add-class! (by-id id) "alert-success"))
    :failed (do
              (remove-class! (by-id id) "alert-success")
              (add-class! (by-id id) "alert-danger"))))

(defn update-element
  "Update the element style class and text"
  [id state msg]
  (toggle-class id state)
  (set-text! (by-id id) msg))

(defn set-ack [status]
  (if (= status 200)
    (update-element ack-el :success (str status " - Successfull Submission"))
    (do
      (update-element ack-el :failed (str status " - Submission Failed"))
      (update-element result-el :failed "Endpoint error"))))

(defn set-result [code msg]
  (cond
    (= code 100) (update-element result-el :success msg)
    (= code 200) (update-element result-el :failed msg)
    (= code 300) (update-element result-el :failed msg)))

(defn call-service []
  (let [str1 (value (by-id "str1"))
        str2 (value (by-id "str2"))
        query-string (str "/scramblies?str1=" str1 "&str2=" str2)]
    (go
      (let [response (<! (http/get query-string))
            {:keys [status body]} response
            {:keys [code msg]} body]
        (set-ack status)
        (set-result code msg)))
    false))

(defn init []
  (let [scramblies-form (.getElementById js/document "scrambliesForm")]
    (set! (.-onsubmit scramblies-form) call-service)))

(set! (.-onload js/window) init)