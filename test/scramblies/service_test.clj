(ns scramblies.service-test
  (:require [clojure.test :refer :all]
            [scramblies.service :as scr]))

(deftest test-scramblies
  (testing "String is lower case?"
    (is (= true (scr/is-lower-case? "world")))
    (is (= false (scr/is-lower-case? "World")))
    (is (= false (scr/is-lower-case? "w2rld"))))

  (testing "Input is valid"
    (is (= true (scr/is-input-valid? scr/is-lower-case? "hello" "world")))
    (is (= false (scr/is-input-valid? scr/is-lower-case? "Hello" "World")))
    (is (= false (scr/is-input-valid? scr/is-lower-case? "H232llo" "World")))
    (is (= false (scr/is-input-valid? scr/is-lower-case? "hj^&%l" "211dsBB"))))

  (testing "Words are scramblies"
    (is (= true (scr/scramble? "lloeh" "hello")))
    (is (= true (scr/scramble? "lloehgggfff" "hello")))
    (is (= true (scr/scramble? "rekqodlw" "world")))
    (is (= true (scr/scramble? "cedewaraaossoqqyt" "codewars")))
    (is (= false (scr/scramble? "katas" "steak")))
    (is (= true (scr/scramble? "hlloe" "ohell")))
    (is (= false (scr/scramble? "Hlloe" "ohell")))
    (is (= false (scr/scramble? "hlloe" "oh3ll")))))
