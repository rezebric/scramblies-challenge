(ns scramblies.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [scramblies.handler :refer :all]))

(deftest test-app
  (testing "default route"
    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (= (:body response) "{\"service\":\"/scramblies\",\"params\":[\"str1\",\"str2\"]}"))))

  (testing "scramblies service success - words are scramblies"
    (let [response (app (mock/request :get "/scramblies?str1=ellho&str2=hello"))
          {:keys [status body]} response]
      (is (= status 200))
      (is (= body "{\"code\":100,\"msg\":\"You did it!!! Words are scramblies.\"}"))))

  (testing "scramblies service failure - words are not scramblies"
    (let [response (app (mock/request :get "/scramblies?str1=elho&str2=hello"))
          {:keys [status body]} response]
      (is (= status 200))
      (is (= body "{\"code\":200,\"msg\":\"Words are not scramblies. Try again.\"}"))))

  (testing "scramblies service invalid input"
    (let [response (app (mock/request :get "/scramblies?str1=ellhoQWE&str2=hello"))
          {:keys [status body]} response]
      (is (= status 200))
      (is (= body "{\"code\":300,\"msg\":\"Invalid input 😝\"}"))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
